openapi: 3.0.0
info:
  title: Nexes AP API for Mobile devices
  description: >-
    This API allows to a user to create new emergency calls through an
    Application Provider
  version: 0.6.2
servers:
  - url: https://localhost
paths:
  /call:
    post:
      tags:
        - EDS
      summary: Creates an EDS with user's call data
      description: >-
        This endpoint is used to send all user's call data from
        server/integrator in a send-and-forget way.
      security:
        - access_token: []
      responses:
        '201':
          description: the EDS with user's information has been created and sent to the PSP
        '400':
          description: Bad request
        '401':
          description: Unauthorized
        default:
          description: Unexpected Error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CallData'
        required: true
  /auth:
    post:
      tags:
        - Auth
      summary: Sign in and Device registration
      description: >-
        Endpoint for sign in from a new device. Also creates a new user account
        if it does not exist.
      security:
        - access_token: []
      responses:
        '204':
          description: Account is valid and verification process is initiated
          headers:
            Set-Cookie:
              description: JSESSIONID that has the session id of the auth request
              schema:
                type: string
        '400':
          description: Bad request
        default:
          description: Unexpected Error
      requestBody:
        $ref: '#/components/requestBodies/Channel'
    put:
      tags:
        - Auth
      summary: Start user channel verification
      description: >-
        Start verification for user channel by sending a code through the given
        channel.
      parameters:
        - name: verify
          in: query
          required: true
          schema:
            type: string
        - name: Cookie
          description: key-value pair with the JSESSIONID cookie recived from /auth
          in: header
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Verification has been succeded
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthResponse'
        '400':
          description: Bad request
        '404':
          description: Auth session does not exist
        default:
          description: Unexpected Error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DeviceValue'
        required: true
  '/users/{userId}':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
    get:
      tags:
        - User Entry
      summary: Find user
      description: Gets User Information
      security:
        - access_token: []
      responses:
        '200':
          description: User information
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
    patch:
      tags:
        - User Entry
      summary: Update user
      description: Modifies all user information
      security:
        - access_token: []
      responses:
        '200':
          description: User information
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '400':
          description: Bad request
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
        required: true
    delete:
      tags:
        - User Entry
      summary: Delete user
      description: Remove user account with all its information
      security:
        - access_token: []
      responses:
        '204':
          description: Success
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
  '/users/{userId}/call':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
    get:
      tags:
        - User Entry
      summary: Creates new call
      description: Creates a new call and open a webSocket for all in call communications
      security:
        - access_token: []
      responses:
        '204':
          description: WebSocket connected
        '401':
          description: Not recognized token authentication
        '404':
          description: Channel does not exist
        default:
          description: Unexpected Error
  '/users/{userId}/channels':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
    get:
      tags:
        - UserChannel Collection
      summary: Channels collection
      description: Gets all the channels from a user
      security:
        - access_token: []
      responses:
        '200':
          description: Gets the channel list from user
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Channel'
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
    post:
      tags:
        - UserChannel Collection
      summary: Initiate channel validation
      description: Initiate the validation process to add a channel
      security:
        - access_token: []
      responses:
        '204':
          description: Account is valid and verification process is initiated
          headers:
            Set-Cookie:
              description: JSESSIONID that has the session id of the auth request
              schema:
                type: string
        '400':
          description: bad request.
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
      requestBody:
        $ref: '#/components/requestBodies/Channel'
    delete:
      tags:
        - UserChannel Collection
      parameters:
        - name: service
          in: query
          required: true
          schema:
            type: string
        - name: id
          in: query
          required: true
          schema:
            type: string
      summary: Removes channel
      description: Removes a channel from a user channels when there are more than one.
      security:
        - access_token: []
      responses:
        '204':
          description: Success!
        '400':
          description: Bad request when wrong body format or only one channel left.
        '401':
          description: Not recognized token authentication
        default:
          description: Unexpected Error
  '/users/{userId}/channels/verify/{code}':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
      - name: code
        in: path
        required: true
        schema:
          type: string
    post:
      tags:
        - UserChannel Collection
      parameters:
        - name: Cookie
          description: key-value pair with the JSESSIONID cookie recived from /auth
          in: header
          required: true
          schema:
            type: string
      summary: Validates channel
      description: >-
        Validates the channel identified by the cookie when the right code is
        used.
      security:
        - access_token: []
      responses:
        '200':
          description: Gets the new Channel
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Channel'
        '401':
          description: Not recognized token authentication
        '404':
          description: Channel does not exist
        default:
          description: Unexpected Error
  '/users/{userId}/devices':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
    get:
      tags:
        - UserDevice Collection
      summary: List of user devices
      description: Gets all registered devices from the user
      security:
        - access_token: []
      responses:
        '200':
          description: List of user devices
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Device'
        '401':
          description: Not recognized token authentication
        '404':
          description: Not Found
        default:
          description: Unexpected Error
  '/users/{userId}/devices/{deviceId}':
    parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
          format: uuid
      - name: deviceId
        in: path
        required: true
        schema:
          type: string
    get:
      tags:
        - UserDevice Entry
      summary: Gets Device
      description: Gets device information
      security:
        - access_token: []
      responses:
        '200':
          description: Device information
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Device'
        '401':
          description: Not recognized token authentication
        '404':
          description: Device does not exist
        default:
          description: Unexpected Error
    delete:
      tags:
        - UserDevice Entry
      summary: Remove one device
      description: Remove one device from the users registred devices
      security:
        - access_token: []
      responses:
        '204':
          description: Remove accion has been succeded
        '401':
          description: Not recognized token authentication
        '404':
          description: Device does not exist
        default:
          description: Unexpected Error
components:
  requestBodies:
    Channel:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Channel'
      required: true
  securitySchemes:
    access_token:
      name: Authorization
      type: apiKey
      in: header
  schemas:
    AuthResponse:
      type: object
      properties:
        id:
          type: string
          description: Verificated channel ID
        token:
          type: string
          description: Authentication token for the user
    User:
      type: object
      properties:
        id:
          type: string
          description: Unique identifcation name for a user.
        privacyRequested:
          type: boolean
          description: >-
            if set to 'true' adherence is determined by the destination
            jurisdiction not the originating jurisdiction.
        familyName:
          type: string
          description: Surname of the user
        givenNames:
          type: string
          description: Names of the users
        preferredName:
          type: string
          description: Name for which the user want to be call.
        prefix:
          type: string
          description: 'prefix over the user name like mr, mrs, or dr'
        suffix:
          type: string
        gender:
          type: string
          enum:
            - MALE
            - FEMALE
            - OTHER
          description: gender definition of the user
        birthdate:
          type: string
          description: User birth date as \'YYYY-MM-DD\'
          pattern: '^(19|20)[0-9]{2}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$'
        address:
          $ref: '#/components/schemas/Address'
        languages:
          $ref: '#/components/schemas/Languages'
        medicalInfo:
          $ref: '#/components/schemas/MedicalInfo'
    Address:
      type: object
      properties:
        street:
          type: string
        locality:
          type: string
        region:
          type: string
        postalCode:
          type: string
        countryName:
          type: string
    Channel:
      type: object
      description: Type of communication channel
      properties:
        id:
          type: string
          description: The user identificator for this specific channel service
        service:
          $ref: '#/components/schemas/ServiceType'
    ChatMessage:
      type: object
      properties:
        timestamp:
          type: string
          format: date-time
        message:
          type: string
    Contact:
      type: object
      properties:
        fullName:
          type: string
        relationship:
          type: string
        contactMethods:
          $ref: '#/components/schemas/Channel'
    Device:
      allOf:
        - properties:
            id:
              type: string
        - $ref: '#/components/schemas/DeviceValue'
    DeviceValue:
      type: object
      properties:
        type:
          type: string
        operatingSystem:
          type: string
        operatingSystemVersion:
          type: string
        model:
          type: string
        name:
          type: string
        pushToken:
          type: string
    Image:
      type: object
      properties:
        format:
          type: string
        content:
          type: string
          format: byte
    Languages:
      description: The user languages information
      type: object
      properties:
        spoken:
          description: List of spoken languages known by the user
          type: array
          items:
            description: >-
              ISO 639-3 Language Codes
              (https://www.ethnologue.com/sites/default/files/LanguageCodes.tab)
            type: string
        written:
          description: List of written languages known to the user
          type: array
          items:
            description: >-
              ISO 639-3 Language Codes
              (https://www.ethnologue.com/sites/default/files/LanguageCodes.tab)
            type: string
        sign:
          description: List of signed languages known by the user
          type: array
          items:
            description: >-
              ISO 639-3 Language Codes
              (https://www.ethnologue.com/sites/default/files/LanguageCodes.tab)
            type: string
    CallData:
      description: User call data
      type: object
      properties:
        accessData:
          $ref: '#/components/schemas/AccessData'
        capabilities:
          $ref: '#/components/schemas/Capabilities'
        channel:
          $ref: '#/components/schemas/Channel'
        location:
          $ref: '#/components/schemas/Circle'
        user:
          $ref: '#/components/schemas/User'
    AccessData:
      description: The access data for this emergency call
      type: object
      properties:
        mobileCodes:
          type: object
          description: >-
            This codes in a combination known as an MCC/MNC tuple, they are used
            to uniquely identify a mobile network operator (ITU E.212)
          properties:
            countryCode:
              type: string
              description: mobile country code or MCC (ITU E.212)
              minLength: 3
              maxLength: 3
              pattern: '^\d{3}'
            networkCode:
              type: string
              description: mobile network code or MCC (ITU E.212)
              minLength: 2
              maxLength: 3
              pattern: '^\d{3}'
        wifi:
          type: string
          description: >-
            Basic Service Set Identifier for Wi-Fi. 48-bit labels that conform
            to MAC-48 conventions
          pattern: '^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$'
    Capabilities:
      description: The available app capabilities for this emergency call
      type: array
      items:
        description: Capability type enumeration
        type: string
        enum:
          - CHAT
          - LOCATION_UPDATE
          - MEDICAL_INFO
          - SETUP_CALL
          - SIP
    Circle:
      type: object
      properties:
        center:
          $ref: '#/components/schemas/Point'
        radius:
          type: number
          format: double
          minimum: 0
          exclusiveMinimum: true
          description: Distance in meters to represent location acuracy
    Point:
      type: object
      properties:
        latitude:
          type: number
          format: double
          pattern: '^[+-]?((90(\.0+)?)|([1-8]?[0-9](\.[0-9]+)?))$'
          description: the latitude as deciman Double DD.DDDDD
        longitude:
          type: number
          format: double
          pattern: '^[+-]?((180(\.0+)?)|(((1[0-7][0-9])|([1-9]?[0-9]))(\.[0-9]+)?))$'
          description: the longitude as deciman Double DDD.DDDDD
    ServiceType:
      type: string
      enum:
        - msisdn
        - skypeName
        - whatsAppId
    MedicalInfo:
      description: Additional medical information.
      type: object
      properties:
        organDonor:
          description: Whether the subscriber is willing to donor his organs or not.
          enum:
            - 'YES'
            - 'NO'
            - UNKNOWN
        bloodType:
          description: Blood type of the user.
          type: string
          enum:
            - A+
            - A-
            - B+
            - B-
            - AB+
            - AB-
            - O+
            - O-
        allergies:
          description: List of the allergens of the user.
          type: array
          items:
            type: string
        medication:
          description: Short description of any current medication treatment.
          type: string
    SocketPackage:
      type: object
      properties:
        type:
          type: string
        content:
          type: object